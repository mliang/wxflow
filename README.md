# wxflow

## 放弃了,去用draw.io了

#### 版本

- 0.0.2

1. 引入Dexie
2. 增加保存和打开功能

- 0.0.1

1. 封装antv/xflow

#### 介绍
> 使用electron封装antv/xflow的流程图解决方案

> antv/xflow地址：https://codesandbox.io/s/ysquyr

> 使用```npx create-react-app wxflow --template typescript```创建React工程

> 最后使用electron创建window壳子

#### 启动

1. npm
```
yarn install
```

2. react启动
```
yarn serve
```

3. react打包
```
yarn bulid
```

4. electron启动
> 需要先启动react，然后修改main.js中mainWindow.loadURL
```
  // and load the index.html of the app.
  // mainWindow.loadURL(url.format({
  //   pathname: path.join(__dirname, './build/index.html'),
  //   protocol: 'file:',
  //   slashes: true
  // }))
  mainWindow.loadURL('http://localhost:3000/')
```
> 在启动electron
```
yarn start
```

3. electron打包
> 需要先打包react，然后修改main.js中mainWindow.loadURL

```
  // and load the index.html of the app.
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, './build/index.html'),
    protocol: 'file:',
    slashes: true
  }))
  // mainWindow.loadURL('http://localhost:3000/')
```
> 在打包electron
```
# 可执行文件
yarn package
# 安装文件
yarn make
```
> 安装目录：C:\Users\...\AppData\Local\wxflow\wxflow.exe，需要手动增加快捷方式

#### 开发说明

antv/xflow的组件有问题，需要修改源码

1. wxflow\node_modules\@antv\xflow-core\es\xflow-main\components\index.d.ts
```
export declare const XFlow: React.FC<React.PropsWithChildren<IProps>>;
```

2. wxflow\node_modules\@antv\xflow-extension\es\flowchart-canvas\index.d.ts
```
export declare const FlowchartCanvas: React.FC<React.PropsWithChildren<IFlowchartGraphProps>>;
```

3. wxflow\node_modules\element-react\typings\index.d.ts  227
``` 
prompt(message: string, title?: string, props?: NextOptions): Promise
```

ps：不清楚是不是React版本的问题。

#### 使用说明

![image](https://gitee.com/mliang/pictures/raw/master/wxflow/wxflow.png)

