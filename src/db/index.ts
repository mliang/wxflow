import Dexie, { Table } from 'dexie'

export interface Graph {
    id?: number;
    type: string;
    name?: string;
    update_time: Date;
    data: string;
}

export class MySubClassedDexie extends Dexie {
    graphs!: Table<Graph>;

    constructor() {
        super('wxflow');
        this.version(1).stores({
            graphs: '++id, type, name, update_time, data',
        });
    }
}

export const db = new MySubClassedDexie();
