
/**
* This is an auto-generated demo by dumi
* if you think it is not working as expected,
* please report the issue at
* https://github.com/umijs/dumi/issues
**/


import ReactDOM from 'react-dom';
import '@antv/xflow/dist/index.css';
import 'antd/dist/antd.min.css';
import '@antv/x6/dist/x6.css';
import App from './App';

const meta = {flowId: ""} 
ReactDOM.render(
    <App meta={meta}/>,
    document.getElementById('root'),
);

