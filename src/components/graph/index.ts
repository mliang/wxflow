import { IModelService, NsModel, RxModel } from '@antv/xflow-core';

// export interface IModel<T> {
//   /** 传入一个回调函数来订阅model的变化 */
//   watch: NsModel.IWatch<T>
//   /** 更新model: 支持传值或者传入更新函数 */
//   setValue: NsModel.ISetValue<T>
//   /** 获取model的值 */
//   getValue: () => T | NsModel.EmptyType
//   /** 是否有非空的值 */
//   hasValidValue: () => boolean
//   /** 通过Promise获取一个非空值 */
//   getValidValue: () => Promise<T>
// }

type Newable<T> = new (...args: any[]) => T;
type Abstract<T> = {
  prototype: T;
};
type Token<T> = string | symbol | Newable<T> | Abstract<T>;

// export const isModelExistUtil = <T>(token: Token<T>) => (modelService: IModelService) => boolean;
/** useModel的Utils */
const getModelUtil = <T>(token: Token<T>) => (modelService: IModelService) => Promise<RxModel<T>>;
/** useModel的Utils */
const useModelValueUtil = <T>(token: Token<T>) => (modelService: IModelService) => Promise<T>;

export namespace GRAPH_CHIOSE {
  export const id = 'GRAPH_CHIOSE'
  export type IState = boolean
  // export const getModel = (modelService: IModelService) => Promise<NsModel.IModel<IState>>;
  export const getModel = getModelUtil<IState>(id)
  export const useValue = useModelValueUtil<IState>(id)
}

